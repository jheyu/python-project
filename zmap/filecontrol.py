"""
This file is for reading and writing files
"""

def readIpAddr(filename="ipaddrList"):
    dstList = []
    file = open(filename, "r")
    for line in file:
        dstList.append(line.strip())
    file.close()
    return dstList

def writeFile(openedTable, filename="output"):
    file = open(filename, "w")
    for ipAddr in openedTable:
        file.writelines(ipAddr+"\n")
    file.close()