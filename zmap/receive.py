"""
This file is for receiving packet
"""

from scapy.all import *

"""
Find ip address from a string where come from "sniff()" function in library scapy
It find src ip address in IP header
arguments:
    inputStr: come from "sniff()" function in library scapy
    type: 2 choice, src or dst ip address in IP header
"""
def findIpAddr(inputStr, type):
    ipStart = inputStr.find("IP")
    if ipStart == -1:
        return None
    if type is "src":
        addrStart = inputStr.find("src=", ipStart)+4 # skip "src="
        addrEnd = inputStr.find(" ", addrStart)
        return inputStr[addrStart:addrEnd]
    elif type is "dst":
        addrStart = inputStr.find("dst=", ipStart)+4 # skip "src="
        addrEnd = inputStr.find(" ", addrStart)
        return inputStr[addrStart:addrEnd]

"""
Scan one packet
Use "sniff()" function in library scapy and set count = 1,
it means only scanning 1 packet every time,
so this function named scan1
It return a string by transforming "sniff()"'s return value
"""
def scan1():
    hostIp = str(IP(dst="168.95.1.1").src)
    try:
        packet = str(sniff(count=1))
        src = findIpAddr(packet, "src") # catch src ip because we want its ack
        # dst = findIpAddr(packet, "dst")
        if not src == hostIp:
            return src
        # if not dst == hostIp:
        #     return dst
    except Exception, e:
        print "Exception:", e
        time.sleep(1) # sleep for one sec
