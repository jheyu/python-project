import filecontrol
import send
import receive
import threading, time
import sys

mymode = "i"
startIpAddr = "140.138.144.1"
endIpAddr = "140.138.144.254"
port = 80
inputFilename = ""
outputFilename = "output"

"""
Main function
"""
def main():
    global openedTable
    openedTable = []

    inputArg(sys.argv) # parsing input arguments

    if mymode == "i":
        global dstList

        print "Mode i: scan ip from range "+startIpAddr+" to "+endIpAddr
        dstList = send.getDstListFromRange(startIpAddr, endIpAddr)
        # print dstList
    elif mymode == "f":
        print "Mode f: scan ip from file with filename:", inputFilename
        readDstList = filecontrol.readIpAddr(inputFilename)
        dstList = send.getDstListFromList(readDstList)
        # print dstList
    elif mymode == "h":
        return None # because help mode
    else:
        print "Error mode!"
        return None


    global sThread
    sThread = threading.Thread(target=sendThread)
    sThread.start()
    rThread = threading.Thread(target=receiveThread)
    rThread.start()

    # block until thread finish
    while sThread.is_alive():
        time.sleep(1)

    time.sleep(5) # wait for receiving

    # check the ip address we send, receive it or not
    writeList = []
    for openedIp in openedTable:
        if openedIp in dstList:
            writeList.append(openedIp)

    # print and output to file
    print "opend address:", writeList
    filecontrol.writeFile(writeList, outputFilename)

"""
Input arguments and parse them
arguments meaning:
    (-i or -f is necessary)
    -i: choose ip range, e.g. -i "192.168.1.1 192.168.1.10"
    -p: choose port number, e.g. -p 80
    -f: choose ip from file with filename, e.g. -f "ipaddrList"
    -o: choose output filename, e.g. -o "output"
"""
def inputArg(argv):
    length = argv.__len__()
    # help
    if length == 2 and argv[1] == "-h":
        mymode = "h"
        print '(-i or -f is necessary)'
        print '-i: choose ip range, e.g. -i "192.168.1.1 192.168.1.10"'
        print '-p: choose port number, e.g. -p 80'
        print '-f: choose ip from file with filename, e.g. -f "ipaddrList"'
        print '-o: choose output filename, e.g. -o "output"'

    # other arguments
    for i in range(1, length):
        if "-" in argv[i]:
            if i+1 < length:
                if argv[i] == "-i":
                    if "-f" in argv:
                        print "-i, -f can't use together"
                        return None
                    global mymode, addrRange, startIpAddr, endIpAddr
                    mymode = "i"
                    addrRange = argv[i+1].strip("\"").split(" ")
                    startIpAddr = addrRange[0]
                    endIpAddr = addrRange[1]
                    print "IP Address Range: "+startIpAddr+" to "+endIpAddr
                elif argv[i] == "-p":
                    global port
                    port = int(argv[i+1])
                    print "Port:", port
                elif argv[i] == "-f":
                    if "-i" in argv:
                        print "-i, -f can't use together"
                        return None
                    mymode = "f"
                    global inputFilename
                    inputFilename = argv[i+1].strip("\"")
                    print "Scan from file, Input filename:", inputFilename
                elif argv[i] == "-o":
                    global outputFilename
                    outputFilename = argv[i+1].strip("\"")
                    print "Output filename:", outputFilename
            else:
                print "You should input arguments after -?"
                return None




"""
A thread definition
Send packets where destination ip address are set by user(from dstList)
"""
def sendThread():
    for dst in dstList:
        send.sendTCP(dst)

"""
A thread definition
Receive all packet, and save the ip address into openedTable
"""
def receiveThread():
    while sThread.is_alive():
        ipAddr = receive.scan1()
        if not ipAddr in openedTable:
            openedTable.append(ipAddr)

"""
Main function called over here
"""
main()