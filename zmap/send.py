"""
This file is for sending packet
"""

from scapy.all import *
import random

"""
Send one TCP packet to specified ip address and port
arguments:
    dst: destination ip address
    dport: destination port, default = 80(HTTP)
"""
def sendTCP(dst, dport=80):
    ip = IP(dst=dst)
    tcp = TCP(dport=dport)
    packet = ip/tcp
    send(packet)

"""
Calculate and randomly generate ip address from the range that user gave
arguments:
    startAddr: start address, e.g. 192.168.1.1
    endAddr: end address, e.g. 192.168.1.10
Return list(dstList)
"""
def getDstListFromRange(startAddr, endAddr):
    starts = startAddr.split(".")
    ends = endAddr.split(".")

    # check syntax
    if not starts.__len__() == 4 or not ends.__len__() == 4:
        print "IP Address syntax(len) error."
        return None
    for i in range(0, 4):
        if starts[i] > ends[i]:
            print "IP Address syntax(start > end) error."
            return None

    dstListInt = []
    for i in range (ipAddrToInt(startAddr), ipAddrToInt(endAddr)+1):
        dstListInt.append(i)
    dstListInt = shuffleAlgorithm(dstListInt)

    dstList = []
    length = dstListInt.__len__()
    for i in range(0, length):
        dstList.append(intToIpAddr(dstListInt[i]))

    return dstList

def getDstListFromList(ipAddrList):
    intList = []
    for eachIpAddr in ipAddrList:
        intList.append(ipAddrToInt(eachIpAddr))

    # random it
    shuffleAlgorithm(intList)

    # transfer back: int to string
    randomList = []
    for eachIntIpAddr in intList:
        randomList.append(intToIpAddr(eachIntIpAddr))

    return randomList

"""
Calculate ip address to integer
Return integer
"""
def ipAddrToInt(ipAddr):
    addrSplit = ipAddr.split(".")
    return int(addrSplit[0])*(2**24)+(int(addrSplit[1]))*(2**16)+int(addrSplit[2])*(2**8)+int(addrSplit[3])

"""
Calculate integer to ip address
Return string(ip address)
"""
def intToIpAddr(number):
    part4 = number % 256
    number /= 256
    part3 = number % 256
    number /= 256
    part2 = number % 256
    number /= 256
    part1 = number
    return str(part1)+"."+str(part2)+"."+str(part3)+"."+str(part4)

"""
Random the list by shuffle algorithm
"""
def shuffleAlgorithm(dstListInt):
    length = dstListInt.__len__()
    for i in range(0, length):
        randNum = random.randint(0,length-1)
        # swap two elements in list
        temp = dstListInt[i]
        dstListInt[i] = dstListInt[randNum]
        dstListInt[randNum] = temp
    return dstListInt