activate_this = 'activate_this.py'
exec(open(activate_this).read(), dict(__file__=activate_this))


#from init import app as application

import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash

app = Flask(__name__)
app.config.from_object('config')

from contextlib import closing

def connect_db():                                                                           
    return sqlite3.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:        
        with app.open_resource('schema.sql') as f:          
            db.cursor().executescript(f.read())
        db.commit()
