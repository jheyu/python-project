import sqlite3 as lite
import sys

con = lite.connect('flaskr.db')

with con:

    cur = con.cursor()
    cur.execute("SELECT * FROM entries")
    rows = cur.fetchall()

    for row in rows:
        print (row)
