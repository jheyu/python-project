# Mini-project #6 - Blackjack

import simplegui
import random

# load card sprite - 936x384 - source: jfitz.com
CARD_SIZE = (72, 96)
CARD_CENTER = (36, 48)
card_images = simplegui.load_image("http://storage.googleapis.com/codeskulptor-assets/cards_jfitz.png")

CARD_BACK_SIZE = (72, 96)
CARD_BACK_CENTER = (36, 48)
card_back = simplegui.load_image("http://storage.googleapis.com/codeskulptor-assets/card_jfitz_back.png")    

# initialize some useful global variables
in_play = False
outcome = "Hit or stand?"
score = 0
dealer_score = 0
player_score = 0

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}


# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [pos[0] + CARD_CENTER[0], pos[1] + CARD_CENTER[1]], CARD_SIZE)
        
# define hand class
class Hand:
    def __init__(self):
        self.cardList = []
        
    def __str__(self):
        result = "Hand contains "
        for card in range(len(self.cardList)):
            result += card.__str__() + " "
        return result
    
    def add_card(self, card):
        self.cardList.append(card)

    def get_value(self):
        hand_value = 0
        countA = 0
        for card in self.cardList:
            rank = card.get_rank()
            if rank == 'A':
                countA += 1
            else:
                hand_value += VALUES[rank]
        
        if countA == 0:
            return hand_value
        else:
            hand_value += countA
            if hand_value + 10 <= 21:
                return hand_value + 10
            else:
                return hand_value
            
    def draw(self, canvas, pos):
        for card in self.cardList:
            card.draw(canvas, pos)
            pos[0] += 80
        
# define deck class 
class Deck:
    def __init__(self):
        self.deckList = []
        for suit in SUITS:
            for rank in RANKS:
                self.deckList.append(Card(suit, rank))

    def shuffle(self): 
        random.shuffle(self.deckList)

    def deal_card(self):
        return self.deckList.pop()
    
    def __str__(self):
        s = "Deck contains "
        for card in range(len(self.deckList)):
            s += card.__str__() + " "
        return s    

#define event handlers for buttons
def deal():
    global outcome, in_play, deck, player_score, dealer_score, player_hand, dealer_hand
    
    outcome = ""
    # your code goes here
    if in_play == True:
        in_play = False
    else:
        deck = Deck()
        deck.shuffle()
    
        player_hand = Hand()
        dealer_hand = Hand()
    
        player_hand.add_card(deck.deal_card())
        player_hand.add_card(deck.deal_card())
    
        dealer_hand.add_card(deck.deal_card())
        dealer_hand.add_card(deck.deal_card())
    
#        player_score = player_hand.get_value()
#        dealer_score = dealer_hand.get_value()
        
        in_play = True
        
def hit():
    global outcome
    
    if player_hand.get_value() < 21:
        player_hand.add_card(deck.deal_card())
    elif player_hand.get_value() > 21:
        outcome = "You have busted"      
        
def stand():
    global outcome, player_score, dealer_score, in_play
    
    in_play = False
    
    if outcome == "You have busted":
        return
    
    while dealer_hand.get_value() < 17:
        dealer_hand.add_card(deck.deal_card())
    
    
    if dealer_hand.get_value() > 21:
        outcome = "Dealer busted. Congratulations!"
#        player_score += 1
    else:
        if dealer_hand.get_value() >= player_hand.get_value() or player_hand.get_value() > 21:
            print "Dealer wins"
            outcome = "Dealer wins. New deal?"
#            dealer_score += 1
        else:
            print "Player wins. New deal?"
            outcome = "Player wins"
#            player_score += 1
            
# draw handler    
def draw(canvas):
    global outcome, in_play, card_back, card_loc, player_score, dealer_score
    
    canvas.draw_text("Blackjack", [220, 50], 50 ,"Pink")

    player_hand.draw(canvas, [100, 300])
    dealer_hand.draw(canvas, [100, 150])
    
    canvas.draw_text(outcome, [10, 100], 30 ,"Pink")

    canvas.draw_text("Dealer", [10, 150], 20 ,"Black")
    canvas.draw_text("Player", [10, 300], 20 ,"Black")
    
    if in_play:
        canvas.draw_image(card_back, CARD_BACK_CENTER, CARD_BACK_SIZE, (136,199), CARD_BACK_SIZE)

# initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)


# get things rolling
deal()
frame.start()


# remember to review the gradic rubric
