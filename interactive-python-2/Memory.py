# implementation of card game - Memory

import simplegui
import random

click1 = 0
click2 = 0

# helper function to initialize globals
def new_game():
    global deck_cards, exposed, turns, state
    state = 0
    turns = 0
    deck_cards = [i%8 for i in range(16)]
    exposed = [False for i in range(16)]
    
    random.shuffle(deck_cards)
    label.set_text("Turns = " + str(turns))
    
# define event handlers
def mouseclick(pos):
    # add game state logic here
    global deck_cards, exposed, turns, state
    global click1, click2
    choice = int(pos[0]/50)
    
    if state == 0:
        exposed[choice] = True
        ckick1 = choice
        turns += 1
        state = 1
        print "click1: " + str(click1)
    elif state == 1:
        exposed[choice] = True
        click2 = choice
        state = 2
        print "click2: " + str(click2)
    elif state == 2:       
        state = 1
        if deck_cards[choice] == deck_cards[click1]:
            exposed[choice] = True
            exposed[click2] = True
        elif deck_cards[choice] == deck_cards[click2]:
            exposed[choice] = True 
            exposed[click1] = True
        else:
            if deck_cards[click1] != deck_cards[click2]:
                exposed[click1] = False
                exposed[click2] = False
            exposed[choice] = True
            click1 = choice
        turns += 1
    label.set_text("Turns = " + str(turns)) 
    
# cards are logically 50x100 pixels in size    
def draw(canvas):
    for i in range(16):
        if exposed[i]:
            canvas.draw_text(str(deck_cards[i]), [50*i, 60], 40, "White")
        else:
            canvas.draw_polygon([[50*i, 0], [50*i, 100], [50*(i+1), 100], [50*(i+1), 0]], 12, 'Yellow')


# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Reset", new_game)
label = frame.add_label("Turns = 0")

# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

# get things rolling
new_game()
frame.start()


# Always remember to review the grading rubric
