"""The Game of Hog."""

from dice import four_sided, six_sided, make_test_dice
from ucb import main, trace, log_current_line, interact

GOAL_SCORE = 100 # The goal of Hog is to score 100 points.

######################
# Phase 1: Simulator #
######################

def roll_dice(num_rolls, dice=six_sided):
    """Roll DICE for NUM_ROLLS times. Return either the sum of the outcomes,
    or 1 if a 1 is rolled (Pig out). This calls DICE exactly NUM_ROLLS times.

    num_rolls:  The number of dice rolls that will be made; at least 1.
    dice:       A zero-argument function that returns an integer outcome.
    """
    # These assert statements ensure that num_rolls is a positive integer.
    assert type(num_rolls) == int, 'num_rolls must be an integer.'
    assert num_rolls > 0, 'Must roll at least once.'
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 1
    i, score, has_one = 0, 0, False
    while i < num_rolls:
        num = dice()
        score += num
        if num == 1:
            has_one = True
        i += 1
    if has_one:
        return 0
    return score
    # END PROBLEM 1

def free_bacon(opponent_score):
    """Return the points scored from rolling 0 dice (Free Bacon)."""
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 2
    return 1 + max(opponent_score//10, opponent_score%10)
    # END PROBLEM 2

def is_prime(n):
    """Return True if a non-negative number N is prime, otherwise return
    False. 1 is not a prime number!
    """
    assert type(n) == int, 'n must be an integer.'
    assert n >= 0, 'n must be non-negative.'
    k = 1
    while k < n:
        if n % k == 0:
            return False
    return True

def next_prime(score):
    while True:
        score += 1
        if is_prime(score):
            return score
        
def take_turn(num_rolls, opponent_score, dice=six_sided):
    """Simulate a turn rolling NUM_ROLLS dice, which may be 0 (Free bacon).

    num_rolls:       The number of dice rolls that will be made.
    opponent_score:  The total score of the opponent.
    dice:            A function of no args that returns an integer outcome.
    """
    assert type(num_rolls) == int, 'num_rolls must be an integer.'
    assert num_rolls >= 0, 'Cannot roll a negative number of dice.'
    assert num_rolls <= 10, 'Cannot roll more than 10 dice.'
    assert opponent_score < 100, 'The game should be over.'
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 2
    if num_rolls == 0:
        score = free_bacon(opponent_score)
    else:
        score = roll_dice(num_rolls, dice)
    if is_prime(score):
        score = next_prime(score)
    return score
    # END PROBLEM 2
    
def select_dice(score, opponent_score):
    """Select six-sided dice unless the sum of SCORE and OPPONENT_SCORE is a
    multiple of 7, in which case select four-sided dice (Hog wild).
    """
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 3
    ssum = score + opponent_score
    if ssum % 7 == 0:
        return four_sided
    return six_sided
    # END PROBLEM 3
        
def other(who):
    """Return the other player, for a player WHO numbered 0 or 1.

    >>> other(0)
    1
    >>> other(1)
    0
    """
    return 1 - who
def is_swap(score0, score1):
    """Returns whether the last two digits of SCORE0 and SCORE1 are reversed
    versions of each other, such as 19 and 91.
    """
    # BEGIN Question 4
    "*** REPLACE THIS LINE ***"
    # END Question 4
    if score0 >= 100:
        score0 -= 100
    if score1 >= 100:
        score1 -= 100
    if score0 % 10 == score1 // 10 and score0 // 10 == score1 % 10:
        return True
    return False

def helper(strategy, player_score, opponent_score, dice = six_sided):
    num_rolls = strategy(player_score, opponent_score)
    score = take_turn(num_rolls, opponent_score, dice)
    # Piggy Back
    if score == 0:
        opponent_score += num_rolls
    player_score += score
    return player_score, opponent_score

def play(strategy0, strategy1, score0=0, score1=0, goal=GOAL_SCORE):
    """Simulate a game and return the final scores of both players, with
    Player 0's score first, and Player 1's score second.

    A strategy is a function that takes two total scores as arguments
    (the current player's score, and the opponent's score), and returns a
    number of dice that the current player will roll this turn.

    strategy0:  The strategy function for Player 0, who plays first
    strategy1:  The strategy function for Player 1, who plays second
    score0   :  The starting score for Player 0
    score1   :  The starting score for Player 1
    """
    who = 0  # Which player is about to take a turn, 0 (first) or 1 (second)
    "*** YOUR CODE HERE ***"
    """
    while score0 < goal and score1 < goal:
        dice = select_dice(score0, score1)
        if who == 0:
            score0, score1 = helper(strategy0, score0, score1, dice)
        else:
            score1, score0 = helper(strategy1, score1, score0, dice)
        # Swine Swap
        if is_swap(score0, score1):
            score0, score1 = score1, score0        
        # Change player
        who = other(who)
    return score0, score1
    """
    # BEGIN PROBLEM 5
    score, opponent_score = 0, 0
    while score0 < goal and opponent_score < goal:
        if who == 0:
            strategy = strategy0
        else:
            strategy = strategy1
        dice = select_dice(score, opponent_score)
        num_rolls = strategy(score0, score1)
        score += take_turn(num_rolls, opponent_score, dice)

        if score ==(2 * opponent_score) or opponent_score == (2 * score):
            score, opponent_score = opponent_score, score
        if score >= goal or opponent_score >= goal:
            break;
        who = other(who)
    if who == 0:
        return score, opponent_score
    else:
        return opponent_score, score
    # END PROBLEM 5
    
#######################
# Phase 2: Strategies #
#######################

def always_roll(n):
    """Return a strategy that always rolls N dice.

    A strategy is a function that takes two total scores as arguments
    (the current player's score, and the opponent's score), and returns a
    number of dice that the current player will roll this turn.

    >>> strategy = always_roll(5)
    >>> strategy(0, 0)
    5
    >>> strategy(99, 99)
    5
    """
    def strategy(score, opponent_score):
        return n
    return strategy

# Experiments

def make_averaged(fn, num_samples=1000):
    """Return a function that returns the average_value of FN when called.

    To implement this function, you will have to use *args syntax, a new Python
    feature introduced in this project.  See the project description.

    >>> dice = make_test_dice(3, 1, 5, 6)
    >>> averaged_dice = make_averaged(dice, 1000)
    >>> averaged_dice()
    3.75
    >>> make_averaged(roll_dice, 1000)(2, dice)
    6.0

    In this last example, two different turn scenarios are averaged.
    - In the first, the player rolls a 3 then a 1, receiving a score of 1.
    - In the other, the player rolls a 5 and 6, scoring 11.
    Thus, the average value is 6.0.
    """
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 6
    def avg(*args):
        res = 0
        for i in range(num_samples):
            res += fn(*args)
        return res/num_samples
    return avg
    # END PROBLEM 6
    
def max_scoring_num_rolls(dice=six_sided):
    """Return the number of dice (1 to 10) that gives the highest average turn
    score by calling roll_dice with the provided DICE.  Assume that dice always
    return positive outcomes.

    >>> dice = make_test_dice(3)
    >>> max_scoring_num_rolls(dice)
    10
    """
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 7
    maxi = 0
    for i in range(1,11):
        avg = make_averaged(roll_dice, 1000)(i, dice)
        if avg > maxi:
            maxi = avg
    return maxi
    # END PROBLEM 7

def winner(strategy0, strategy1):
    """Return 0 if strategy0 wins against strategy1, and 1 otherwise."""
    score0, score1 = play(strategy0, strategy1)
    if score0 > score1:
        return 0
    else:
        return 1

def average_win_rate(strategy, baseline=always_roll(5)):
    """Return the average win rate (0 to 1) of STRATEGY against BASELINE."""
    win_rate_as_player_0 = 1 - make_averaged(winner)(strategy, baseline)
    win_rate_as_player_1 = make_averaged(winner)(baseline, strategy)
    return (win_rate_as_player_0 + win_rate_as_player_1) / 2 # Average results

def run_experiments():
    """Run a series of strategy experiments and report results."""
    if True: # Change to False when done finding max_scoring_num_rolls
        six_sided_max = max_scoring_num_rolls(six_sided)
        print('Max scoring num rolls for six-sided dice:', six_sided_max)
        four_sided_max = max_scoring_num_rolls(four_sided)
        print('Max scoring num rolls for four-sided dice:', four_sided_max)

    if False: # Change to True to test always_roll(8)
        print('always_roll(8) win rate:', average_win_rate(always_roll(8)))

    if False: # Change to True to test bacon_strategy
        print('bacon_strategy win rate:', average_win_rate(bacon_strategy))

    if False: # Change to True to test prime_strategy
        print('prime_strategy win rate:', average_win_rate(prime_strategy))

    if False: # Change to True to test final_strategy
        print('final_strategy win rate:', average_win_rate(final_strategy))

    "*** You may add additional experiments as you wish ***"

# Strategies

def bacon_strategy(score, opponent_score, margin=8, num_rolls=5):
    """This strategy rolls 0 dice if that gives at least MARGIN points,
    and rolls NUM_ROLLS otherwise.
    """
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 8
    if max(opponent_score//10, opponent%10)+1 > margin:
        return 0
    else:
        return num_rolls
    # END PROBLEM 8

def hogtimusprime_strategy(score, opponent_score, margin=8, num_rolls=5):
    """This strategy rolls 0 dice when it results in a beneficial boost and
    rolls NUM_ROLLS if rolling 0 dice gives the opponent a boost. It also
    rolls 0 dice if that gives at least MARGIN points and rolls NUM_ROLLS
    otherwise.
    """
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 9
    sscore = 0
    dice = select_dice(score, opponent_score)
    sscore += take_turn(num_rolls, opponent_score, dice)
    if is_prime(sscore + score + opponent_score):
        if who == 0:
            score += sscore
        else:
            opponent_score += sscore
    if score > opponent_score:
        score += sscore
    else:
        opponent_score += sscore   
    # END PROBLEM 9

def swap_strategy(score, opponent_score, num_rolls=5):
    """This strategy rolls 0 dice when it would result in a beneficial swap and
    rolls BASELINE_NUM_ROLLS if it would result in a harmful swap. It also rolls
    0 dice if that gives at least BACON_MARGIN points and rolls
    BASELINE_NUM_ROLLS otherwise.
    >>> swap_strategy(23, 60) # 23 + (1 + max(6, 0)) = 30: Beneficial swap
    0
    >>> swap_strategy(27, 18) # 27 + (1 + max(1, 8)) = 36: Harmful swap
    5
    >>> swap_strategy(50, 80) # (1 + max(8, 0)) = 9: Lots of free bacon
    0
    >>> swap_strategy(12, 12) # Baseline
    5
    """
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 9
    play_zero = score + max(opponent_score//10,opponent_score%10) + 1
    if play_zero == (2*opponent_score) or opponent_score == (2*play_zero):
        if play_zero < opponent_score:
            return 0
        else:
            return num_rolls
    else:
        return bacon_strategy(play_zero, opponent_score)
    # END PROBLEM 9
    
def final_strategy(score, opponent_score):
    """Write a brief description of your final strategy.

    *** YOUR DESCRIPTION HERE ***
    """
    "*** YOUR CODE HERE ***"
    # BEGIN PROBLEM 10
    return swap_strategy(score, opponent_score)
    # END PROBLEM 10


##########################
# Command Line Interface #
##########################

# Note: Functions in this section do not need to be changed.  They use features
#       of Python not yet covered in the course.


@main
def run(*args):
    """Read in the command-line argument and calls corresponding functions.

    This function uses Python syntax/techniques not yet covered in this course.
    """
    import argparse
    parser = argparse.ArgumentParser(description="Play Hog")
    parser.add_argument('--run_experiments', '-r', action='store_true',
                        help='Runs strategy experiments')
    args = parser.parse_args()

    if args.run_experiments:
        run_experiments()
