# Testing template for "Guess the number"

###################################################
# Student should add code for "Guess the number" here


# template for "Guess the number" mini-project
# input will come from buttons and an input field
# all output for the game will be printed in the console


import simplegui
import random
import math

# initialize global variables used in your code
num_range = 100
guess_left = 0
secret_number = 0

# helper function to initial game
def new_game():
    global num_range
    global guess_left
    global secret_number
    
    secret_number = random.randrange(0, num_range)
    
    if num_range == 100:
        guess_left = 7
    elif num_range == 1000:
        guess_left = 10
        
    print "New game. The range is from 0 to", num_range, ". Good luck!"
    print "Number of remaining guesses is", guess_left, "\n"
    pass

# define callback functions for control panel
def range100():
    global num_range
    num_range = 100
    new_game()
    pass

def range1000():
    global num_range
    num_range = 1000
    new_game()
    pass

def get_input(guess):
    global guess_left
    global secret_number
   
    print "Guess was", guess
    guess_left = guess_left - 1
    print "Number of remaining guesses is", guess_left
    
    if guess_left == 0:
        print "You ran out of guesses. The number was", secret_number
        new_game()
    
    if guess < secret_number: 
        print "Higher!"
    elif guess == secret_number:
        print "Correct!"
    else:
        print "Lower!"
    pass

# create window(s)
f = simplegui.create_frame("Guess the number", 200, 200)

# create control elements for window
f.add_button("Range is [0, 100)", range100, 200)
f.add_button("Range is [0, 1000)", range1000, 200)
f.add_input("Enter a guess", get_input, 200)

new_game()
f.start()

###################################################
# Start our test #1 - assume global variable secret_number
# is the the "secret number" - change name if necessary


#input_guess("50")
#input_guess("75")
#input_guess("62")
#input_guess("68")
#input_guess("71")
#input_guess("73")
#input_guess("74")

###################################################
# Output from test #1
#New game. Range is [0,100)
#Number of remaining guesses is 7
#
#Guess was 50
#Number of remaining guesses is 6
#Higher!
#
#Guess was 75
#Number of remaining guesses is 5
#Lower!
#
#Guess was 62
#Number of remaining guesses is 4
#Higher!
#
#Guess was 68
#Number of remaining guesses is 3
#Higher!
#
#Guess was 71
#Number of remaining guesses is 2
#Higher!
#
#Guess was 73
#Number of remaining guesses is 1
#Higher!
#
#Guess was 74
#Number of remaining guesses is 0
#Correct!
#
#New game. Range is [0,100)
#Number of remaining guesses is 7

###################################################
# Start our test #2 - assume global variable secret_number
# is the the "secret number" - change name if necessary

#range1000()
#secret_number = 375	
#input_guess("500")
#input_guess("250")
#input_guess("375")

###################################################
# Output from test #2
#New game. Range is [0,100)
#Number of remaining guesses is 7
#
#New game. Range is [0,1000)
#Number of remaining guesses is 10
#
#Guess was 500
#Number of remaining guesses is 9
#Lower!
#
#Guess was 250
#Number of remaining guesses is 8
#Higher!
#
#Guess was 375
#Number of remaining guesses is 7
#Correct!
#
#New game. Range is [0,1000)
#Number of remaining guesses is 10



###################################################
# Start our test #3 - assume global variable secret_number
# is the the "secret number" - change name if necessary

#range100()
#secret_number = 28	
#input_guess("50")
#input_guess("50")
#input_guess("50")
#input_guess("50")
#input_guess("50")
#input_guess("50")
#input_guess("50")

###################################################
# Output from test #3
#New game. Range is [0,100)
#Number of remaining guesses is 7
#
#Guess was 50
#Number of remaining guesses is 6
#Lower!
#
#Guess was 50
#Number of remaining guesses is 5
#Lower!
#
#Guess was 50
#Number of remaining guesses is 4
#Lower!
#
#Guess was 50
#Number of remaining guesses is 3
#Lower!
#
#Guess was 50
#Number of remaining guesses is 2
#Lower!
#
#Guess was 50
#Number of remaining guesses is 1
#Lower!
#
#Guess was 50
#Number of remaining guesses is 0
#You ran out of guesses.  The number was 28
#
#New game. Range is [0,100)
#Number of remaining guesses is 7
