# template for "Stopwatch: The Game"
import simplegui

# define global variables
WIDTH = 200
HEIGHT = 200
count = 0
message = "0:00.0"

# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(t):
    global message
    tenth_sec = (t) % 10
    sec = int(t / 10) % 10
    minutes = int(t / 600) % 600
    ten_min = int(t / 100) % 6
    message = str(minutes) + ":" + str(ten_min) + str(sec) + "." + str(tenth_sec)
    
# define event handlers for buttons; "Start", "Stop", "Reset"
def start():
    timer.start()
    
def stop():
    timer.stop()
    
def reset():
    global count
    count = 0
    format(count)
    
# define event handler for timer with 0.1 sec interval
def tick():
    global count
    count += 1
    format(count)
    
# define draw handler
def draw(canvas):
    canvas.draw_text(message, [50, 100], 36, "Red")
    
# create frame
frame = simplegui.create_frame("Home", WIDTH, HEIGHT)
timer = simplegui.create_timer(100, tick)
frame.add_button("Start", start, 100)
frame.add_button("Stop", stop, 100)
frame.add_button("Reset", reset, 100)

# register event handlers
frame.set_draw_handler(draw)

# start frame
frame.start()

# Please remember to review the grading rubric
